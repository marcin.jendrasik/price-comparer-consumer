FROM openjdk:11.0.5-jre-slim
RUN mkdir /app
COPY . /app
WORKDIR /app/build/libs
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","consumer-0.0.1-SNAPSHOT.jar"]
