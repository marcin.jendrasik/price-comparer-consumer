var tableBuilder = (function () {
    var remove = function () {
        var tbody = document.getElementById("tbody");
        while (tbody.firstChild) {
            tbody.removeChild(tbody.lastChild);
        }
    };

    var build = function(response) {
        remove();
        response.forEach(function (e) {
            var id = document.createElement("td");
            id.innerText = e.id;

            var title = document.createElement("td");
            var titleA = document.createElement("a");
            titleA.setAttribute("href", e.link);
            titleA.innerText = e.title;

            title.appendChild(titleA);

            var link = document.createElement("td");
            var linkA = document.createElement("a");
            linkA.setAttribute("href", e.link);

            var linkImg = document.createElement("img");
            linkImg.setAttribute("src", e.img);

            linkA.appendChild(linkImg);
            link.appendChild(linkA);

            var price = document.createElement("td");
            price.innerText = e.price;

            var type = document.createElement("td");

            if (e.type === "APTEKA_GEMINI") {
                type.innerText = "APTEKA GEMINI";
            }

            var row = document.createElement("tr");
            row.appendChild(id);
            row.appendChild(title);
            row.appendChild(link);
            row.appendChild(price);
            row.appendChild(type);

            var tbody = document.getElementById("tbody");
            tbody.appendChild(row);
        });
    };

    return {build: build}
})();

var productFetcher = (function () {
    var getProducts = function (url, param) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.status === 200) {
                var response = JSON.parse(xmlHttp.responseText);
                tableBuilder.build(response);

            } else if (xmlHttp.status === 400) {
                console.log("Something went wrong");
            } else {
                console.log("Another cause occurred")
            }
        };

        xmlHttp.open("GET", url + "?query=" + param, true);
        xmlHttp.send();
    };

    var fetch = function () {
        var form = document.getElementById("search-product");
        form.addEventListener("submit", function (ev) {
            ev.preventDefault();
            var param = document.getElementById("search").value;
            getProducts(form.getAttribute("action"), param);
        });
    };

    return {
        fetch: fetch,
        getProducts: getProducts}
})();

var productInitializer = (function() {
    var initialize = function () {
        var form = document.getElementById("search-product");
        productFetcher.getProducts(form.getAttribute("action"), "a");
    }

    return {initialize: initialize}
})();

productInitializer.initialize();
productFetcher.fetch();
