package com.pricecomparer.consumer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "kafka-config")
@Data
public class KafkaConfigData {

    private String bootstrapAddress;
    private String schemaRegistryUrlKey;
    private String schemaRegistryUrl;
}
