package com.pricecomparer.consumer.persistence.repository;

import com.pricecomparer.model.domain.Product;
import com.pricecomparer.model.domain.ProductType;

import java.util.List;

public interface ProductRepository {

    void saveAll(List<Product> products);
    List<Product> getAll(final String query);
    void deleteAll(ProductType productType);
}
