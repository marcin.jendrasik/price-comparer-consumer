package com.pricecomparer.consumer.persistence.repository;

import com.pricecomparer.consumer.mapper.ProductMapper;
import com.pricecomparer.consumer.persistence.document.ProductDocument;
import com.pricecomparer.model.domain.Product;
import com.pricecomparer.model.domain.ProductType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Repository
@RequiredArgsConstructor
public class ProductRepositoryImpl implements ProductRepository {

    private final ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Override
    public void saveAll(final List<Product> products) {
        elasticsearchRestTemplate.save(products.stream()
                .map(ProductMapper::toDocument)
                .collect(Collectors.toList()));
        elasticsearchRestTemplate.indexOps(ProductDocument.class).refresh();
    }

    @Override
    public List<Product> getAll(final String query) {
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchPhrasePrefixQuery("title", Optional.ofNullable(query).orElse("")))
                .build();

        return elasticsearchRestTemplate.search(searchQuery, ProductDocument.class,
                IndexCoordinates.of("product")).stream()
                .map(SearchHit::getContent)
                .map(ProductMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAll(final ProductType productType) {
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQuery().must(matchQuery("type", productType)))
                .build();

        elasticsearchRestTemplate.delete(searchQuery, ProductDocument.class, IndexCoordinates.of("product"));
    }
}
