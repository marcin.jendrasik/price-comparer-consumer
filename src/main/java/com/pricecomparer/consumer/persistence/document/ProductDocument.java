package com.pricecomparer.consumer.persistence.document;

import com.pricecomparer.model.domain.ProductType;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "product")
@Builder
@Getter
public class ProductDocument {

    @Id
    private String id;
    private String title;
    private String img;
    private String price;
    private String link;
    private ProductType type;
}
