package com.pricecomparer.consumer.mapper;

import com.pricecomparer.consumer.persistence.document.ProductDocument;
import com.pricecomparer.model.domain.Product;
import com.pricecomparer.model.domain.ProductType;
import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class ProductMapper {

    public static Product toDomain(com.pricecomparer.model.avro.Product product) {
        return Optional.ofNullable(product)
                .map(p -> Product.builder()
                        .id(p.getId())
                        .title(p.getTitle())
                        .img(p.getImg())
                        .price(p.getPrice())
                        .link(p.getLink())
                        .type(ProductType.valueOf(p.getType().name()))
                        .build())
                .orElse(null);
    }

    public static ProductDocument toDocument(final Product product) {
        return Optional.ofNullable(product)
                .map(p -> ProductDocument.builder()
                        .id(p.getId().toString())
                        .title(p.getTitle())
                        .img(p.getImg())
                        .price(p.getPrice())
                        .link(p.getLink())
                        .type(p.getType())
                        .build())
                .orElse(null);
    }

    public static Product toDomain(final ProductDocument productDocument) {
        return Optional.ofNullable(productDocument)
                .map(pd -> Product.builder()
                        .id(Long.valueOf(pd.getId()))
                        .title(pd.getTitle())
                        .img(pd.getImg())
                        .price(pd.getPrice())
                        .link(pd.getLink())
                        .type(pd.getType())
                        .build())
                .orElse(null);
    }
}
