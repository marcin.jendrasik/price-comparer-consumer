package com.pricecomparer.consumer.controller;

import com.pricecomparer.consumer.service.ProductService;
import com.pricecomparer.model.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductRestController {

    private final ProductService productService;

    @GetMapping("/search")
    public List<Product> getAll(final String query) {
        return productService.getAll(query);
    }
}
