package com.pricecomparer.consumer.service;

import com.pricecomparer.consumer.persistence.repository.ProductRepository;
import com.pricecomparer.model.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public void saveAll(final List<Product> products) {
        productRepository.saveAll(products);
    }

    public List<Product> getAll(final String query) {
        return productRepository.getAll(query);
    }
}
