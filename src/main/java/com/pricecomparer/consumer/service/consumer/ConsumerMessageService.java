package com.pricecomparer.consumer.service.consumer;

import com.pricecomparer.consumer.mapper.ProductMapper;
import com.pricecomparer.consumer.service.ProductService;
import com.pricecomparer.model.avro.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConsumerMessageService {

    private static final int BATCH_SIZE = 10;
    private final ProductService productService;

    @KafkaListener(
            groupId = "${kafka-consumer-config.group-id}",
            topics = "${kafka-topic-config.create-product}",
            containerFactory = "productKafkaListenerContainerFactory")
    public void listenForProducts(final List<Product> products) {
        products.forEach(product -> log.info("Received product : {}", product.toString()));

        int size = products.size();
        int pageCount = (int) Math.ceil((double) size / BATCH_SIZE);

        IntStream.range(0, pageCount)
                .mapToObj(page -> products.subList(page * BATCH_SIZE, Math.min((page + 1) * BATCH_SIZE, size)))
                .map(subList -> subList.stream().map(ProductMapper::toDomain).collect(Collectors.toList()))
                .forEach(productService::saveAll);
    }
}
