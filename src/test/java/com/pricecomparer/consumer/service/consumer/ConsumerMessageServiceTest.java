package com.pricecomparer.consumer.service.consumer;

import com.pricecomparer.consumer.service.ProductService;
import com.pricecomparer.model.avro.Product;
import com.pricecomparer.model.avro.ProductType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ConsumerMessageServiceTest {

    @Autowired
    private ConsumerMessageService consumerMessageService;

    @Autowired
    private ProductService productService;

    @Test
    void givenListOfProducts_whenReceived_thenSaveThemAll() {
        //given
        List<Product> products = new ArrayList<>(Arrays.asList(
            new Product(1L, "title1", "img1", "price1", "link1", ProductType.APTEKA_GEMINI),
            new Product(2L, "title2", "img2", "price2", "link2", ProductType.APTEKA_GEMINI)
        ));

        //when
        consumerMessageService.listenForProducts(products);
        final List<com.pricecomparer.model.domain.Product> savedProducts = productService.getAll("title");

        //then
        assertFalse(savedProducts.isEmpty());
        assertEquals(products.get(0).getId(), savedProducts.get(0).getId());
        assertEquals(products.get(0).getTitle(), savedProducts.get(0).getTitle());
        assertEquals(products.get(0).getImg(), savedProducts.get(0).getImg());
        assertEquals(products.get(0).getPrice(), savedProducts.get(0).getPrice());
        assertEquals(products.get(0).getLink(), savedProducts.get(0).getLink());
        assertEquals(products.get(0).getType().name(), savedProducts.get(0).getType().name());
    }
}