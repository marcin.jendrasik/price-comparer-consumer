# Price Comparer Consumer
This microservice app is one of the parts of Price Comparer app. Price Comparer consists of:

- [Price Comparer Producer](https://gitlab.com/marcin.jendrasik/price-comparer-producer)
- Price Comparer Consumer

## Introduction
This microservice will be responsible for getting messages from Kafka queue and sharing search-engine in order to show and compare products from pharmacies.
Kafka Queued messages will appear through another app [Price Comparer Producer](https://gitlab.com/marcin.jendrasik/price-comparer-producer).

## Technologies
- OpenJDK v11.0.5
- Spring Boot Framework v2.3.4
- Elasticsearch v7.9.3
- Zookeeper/Kafka
- Minikube v1.12.3
- Docker v18.09.2
- Docker Compose v1.21.2

## Prerequisites
- Familiar with docker and docker-compose
- Familiar with Minikube or Kubernetes
- Helm v3.4.0
- Minikube v1.12.3
- Docker v18.09.2
- Docker Compose v1.21.2

## Run application by docker-compose
Go to the section [Run applications by docker-compose](https://gitlab.com/marcin.jendrasik/price-comparer-producer#run-applications-by-docker-compose).

## Run application on Minikube cluster
Go to the section [Run applications on Minikube cluster](https://gitlab.com/marcin.jendrasik/price-comparer-producer#run-applications-on-minikube-cluster).